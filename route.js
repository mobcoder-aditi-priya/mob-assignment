var express     = require('express');
var router      = express.Router();


const userRoute     = require('./user/userRoute');
const boardRoute    = require('./board/boardRoute');
const taskRoute    = require('./task/taskRoute');


// router.use('/user', userRoute);
// router.use('/board', boardRoute);
// router.use('/task', taskRoute);

// module.exports = router;





module.exports = function (app) {
    app.use('/user', userRoute);
    app.use('/board', boardRoute);
    app.use('/task', taskRoute);
}