const userRoute = require("express").Router();
const bodyparser = require('body-parser');

const userFacade = require("./userFacade");
const validators = require("./userValidators");


userRoute.route("/signup")
    .post(function (req, res) {

        let { email, password } = req.body;
        userFacade.signup({ email, password })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });





userRoute.route("/login")
    .post(function (req, res) {

        let { email, password } = req.body;
        userFacade.login({ email, password })
            .then(function (result) {
                res.status(200).send(result)
            }).catch(function (err) {
                res.status(400).send(err)
            })
    });





module.exports = userRoute;
























