// Importing mongoose
var mongoose = require("mongoose");

var Schema = mongoose.Schema;
var User;

var UserSchema = new Schema({
        
        email: {type: String, required: true, unique: true}, 
        password:{type:String,default: ''},
        isVerified: {type: Number, default: 1},
        status: {type: Number, default: 1},
        created: {type: Date, default: Date.now},
        updated: {type: Date, default: Date.now},
        isDeleted: {type: Number, default: 0},
    });

//Export admin module
User = module.exports = mongoose.model('user', UserSchema);
