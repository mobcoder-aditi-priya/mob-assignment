"use strict";
const jwt = require("jsonwebtoken");



const userService = require("./userService");
const userMapper = require("./userMapper");



async function signup(params) {
    let emailExist = await userService.isEmailIdExist(params)

    if (emailExist) {
        throw "email is already registered"
    }

    else {
        return userService.signup(params)

            .then(function (user) {
                return userMapper.signupMapping(user);
            })

    }

}

function login(loginInfo) {
    let emailExist = userService.isEmailIdExist(loginInfo)
    if (!emailExist) {
        throw "email not found"
    }
    else {
        return userService.login(loginInfo)
            .then(async function (result) {
                if (!result) {
                    throw "password incorrect"
                }
            else {
                const token = jwt.sign( result.email ,result.password);
                result.token = token;
                return await result
            }

            })

            .then(function (response) {
                return userMapper.loginMapping(response)
            })
    }

}




module.exports = {
    signup,
    login

};

