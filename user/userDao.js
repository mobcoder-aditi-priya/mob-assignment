"use strict";

var mongoose = require("mongoose");

const User = require('./userModel');


let BaseDao = require('../dao/baseDao');
const userDao = new BaseDao(User);

var md5 = require('md5');


function signup(userInfo) {
    userInfo.password= md5(userInfo.password)

    let user = new User(userInfo);
    return userDao.save(user);
}


async function  login(params){

    let query = {};
    query.email = params.email;
    query.password = md5(params.password);
    return await userDao.findOne(query)
 }

function  isEmailIdExist(params){

    let query = {}; 
    query.email = params.email
    return userDao.findOne(query)
 }

 function  isuserIdExist(params){

  let query = {}; 
  query._id = params.userId
  return userDao.findOne(query)
}


module.exports = {
  signup,
  login,
  isEmailIdExist,
  isuserIdExist

};

