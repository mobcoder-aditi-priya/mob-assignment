"use strict";


const userDao = require('./userDao');



function signup(params){
    return userDao.signup(params);
}

function login(params){
    return userDao.login(params);
}
function isEmailIdExist(params){
    return userDao.isEmailIdExist(params);
}

function isuserIdExist(params){
    return userDao.isuserIdExist(params);
}

module.exports = {
   signup,
   login,
   isEmailIdExist,
   isuserIdExist


};

