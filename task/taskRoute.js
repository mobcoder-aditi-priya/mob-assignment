const taskRoute = require("express").Router();
const bodyparser = require('body-parser');

const taskFacade = require("./taskFacade");



taskRoute.route("/create")
    .post(function (req, res) {
        let { boardId, taskName, currentStatus } = req.body;
        taskFacade.create({ boardId, taskName, currentStatus })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });
;

taskRoute.route("/listTask")
    .get(function (req, res) {

        let { boardId } = req.query;
        taskFacade.listTask({ boardId })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });


taskRoute.route("/changeTaskStatus")
    .post(function (req, res) {
        let { taskId, newStatus, currentStatus } = req.body;
        
        taskFacade.changeTaskStatus({ taskId, newStatus, currentStatus })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });
;

module.exports = taskRoute;
























