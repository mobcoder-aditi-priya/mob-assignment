// Importing mongoose
var mongoose = require("mongoose");
const boardModel = require("../board/boardModel");


var Schema = mongoose.Schema;
var Task;

var TaskSchema = new Schema({

    boardId: { type: Schema.Types.ObjectId, ref: boardModel, required: true },
    taskName: { type: String },
    currentStatus: { type: Number, min: 1, max: 4, default: 1 },//1 =‘To do’,‘In Progress’,2=‘On hold’,3=‘Completed’,4=‘Released’
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    isDeleted: { type: Number, default: 0 },
});

//Export admin module
Task = module.exports = mongoose.model('Task', TaskSchema);
