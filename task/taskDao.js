"use strict";

var mongoose = require("mongoose");

const Task = require('../task/taskModel');


let BaseDao = require('../dao/baseDao');
const taskDao = new BaseDao(Task);



function create(params) {
    let task = new Task(params);
    return taskDao.save(task)
}


function listTask(params) {

    let query = {};
    let id = mongoose.Types.ObjectId(params.boardId);
    query.boardId = id
    return taskDao.find(query)

}


function changeTaskStatus(params) {

    let query = {};
    let update = {};
    let id = mongoose.Types.ObjectId(params.taskId);
    query._id = id
    query.currentStatus =params.currentStatus
    update.currentStatus=params.newStatus
    let options={}
    options.new=true
    return taskDao.findOneAndUpdate(query,update,options)

}


function istaskIdExist(params) {

    let query = {};
    let id = mongoose.Types.ObjectId(params.taskId);
    query._id = id
    return taskDao.find(query)

}

module.exports = {
    create,
    listTask,
    changeTaskStatus,
    istaskIdExist

};

