"use strict";


const boardService = require("../board/boardService");
const taskService = require("./taskService");
const taskMapper = require("./taskMapper");



async function create(params) {
    let boardIdExist = await boardService.isboardIdExist(params)

    if (!boardIdExist) {
        throw "board not found"
    }

    else {
        
        return taskService.create(params)

            .then(function (task) {
                return taskMapper.createMapping(task);
            })

    }

}




async function listTask(params) {

    let boardIdExist = await boardService.isboardIdExist(params)

    if (!boardIdExist) {
        throw "board not found"
    }

    else {
      
        return taskService.listTask(params)

            .then(function (task) {
                return taskMapper.listTasksMapping(task);
            })

    }

}


async function changeTaskStatus(params) {

    let taskIdExist = await taskService.istaskIdExist(params)

    if (!taskIdExist) {
        throw "task not found"
    }

    else {
      
        return taskService.changeTaskStatus(params)

            .then(function (task) {
                return taskMapper.changeTaskStatusMapping(task);
            })

    }

}

module.exports = {
    create,
    listTask,
    changeTaskStatus

};

