

 function createMapping(params) {

    var respObj;
    respObj = {

        "message": "create successfully",
        "taskDetails": params
    }
    return respObj
}
 


function listTasksMapping(params) {

    var respObj;
    respObj = {

        "message": "listed successfully",
        "taskDetails": params
    }
    return respObj
}
 
function changeTaskStatusMapping(params) {

    var respObj;
    respObj = {

        "message": "status changed successfully",
        "taskDetails": params
    }
    return respObj
}
 module.exports = {

    createMapping,
    listTasksMapping,
    changeTaskStatusMapping
 
 }