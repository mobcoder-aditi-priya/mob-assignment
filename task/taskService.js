"use strict";


const taskDao = require('./taskDao');


function create(params){
    return taskDao.create(params);
}

function listTask(params){
  return taskDao.listTask(params);
}

function changeTaskStatus(params){
    return taskDao.changeTaskStatus(params);
  }


function istaskIdExist(params){
    return taskDao.istaskIdExist(params);
  }
module.exports = {
  create,
  listTask,
  changeTaskStatus,
  istaskIdExist


};

