const boardRoute = require("express").Router();
const bodyparser = require('body-parser');
const jsonwebtoken = require('jsonwebtoken')


// const constants = require("../../../constant");
const boardFacade = require("./boardFacade");



boardRoute.route("/create")
    .post(function (req, res) {

        let { userId, boardName, colour } = req.body;
        boardFacade.create({ userId, boardName, colour })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });


boardRoute.route("/listBoard")
    .get(function (req, res) {
    
        let { userId } = req.query;
        boardFacade.listBoard({ userId })
            .then(function (result) {
                res.status(200).send(result)

            }).catch(function (err) {
                res.status(400).send(err)
            })
    });


module.exports = boardRoute;
























