"use strict";


const userService = require("../user/userService");
const boardService = require("./boardService");
const boardMapper = require("./boardMapper");



async function create(params) {
    let userExist = await userService.isuserIdExist(params)

    if (!userExist) {
        throw "user not found"
    }

    else {
        return boardService.create(params)

            .then(function (user) {
                return boardMapper.createMapping(user);
            })

    }

}




async function listBoard(params) {
    let userExist = await userService.isuserIdExist(params)

    if (!userExist) {
        throw "user not found"
    }

    else {
        return boardService.listBoard(params)

            .then(function (user) {
                return boardMapper.listBoardMapping(user);
            })

    }

}


module.exports = {
    create,
    listBoard

};

