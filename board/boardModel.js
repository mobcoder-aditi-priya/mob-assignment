// Importing mongoose
var mongoose = require("mongoose");

const userModel = require("../user/userModel");

var Schema = mongoose.Schema;
var Board;

var BoardSchema = new Schema({
        
        boardName: {type: String, required: true},
        colour:{type:String, required: true},
        userId:{ type:Schema.Types.ObjectId, ref:userModel,required:true },
        isVerified: {type: Number, default: 1},
        status: {type: Number, default: 1},
        created: {type: Date, default: Date.now},
        updated: {type: Date, default: Date.now},
        isDeleted: {type: Number, default: 0},
    });

//Export admin module
Board = module.exports = mongoose.model('Board', BoardSchema);
