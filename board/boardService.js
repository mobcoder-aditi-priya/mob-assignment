"use strict";


const boardDao = require('./boardDao');


function create(params){
    return boardDao.create(params);
}

function listBoard(params){
  return boardDao.listBoard(params);
}

function isboardIdExist(params){
  return boardDao.isboardIdExist(params);
}


module.exports = {
  create,
  listBoard,
  isboardIdExist


};

