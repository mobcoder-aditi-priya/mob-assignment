"use strict";

var mongoose = require("mongoose");

const Board = require('../board/boardModel');


// init user dao
let BaseDao = require('../dao/baseDao');
const boardDao = new BaseDao(Board);



function create(params) {

    let board = new Board(params);
    return boardDao.save(board);
}


function listBoard(params) {

  let query={};
  let id = mongoose.Types.ObjectId(params.userId);
  query.userId= id
  return boardDao.find(query)

}


function isboardIdExist(params) {

  let query={};
  let id = mongoose.Types.ObjectId(params.boardId);
  query._id= id
  return boardDao.find(query)

}

module.exports = {
  create,
  listBoard,
  isboardIdExist

};

