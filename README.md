 User signup and login apis :
 User can sign up using email and password
 Password saved in db is encrypted by md5 and generate accesstoken on login ( created token not working properly)

 Create Board api:
 A user can create board using userId ,board name ,colour 

 List Board api:
 A user can get list of boards using userId

 Create task api:
 A user can create task using boardId, taskName, currentStatus

 List task api:
 A user can get list of tasks of a board using boardId as input 

 Change task status:
 A user can change task current status using taskId , current status , new status of a task
